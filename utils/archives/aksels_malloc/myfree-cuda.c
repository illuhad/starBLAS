#include <cuda_runtime_api.h>
#include <stdio.h>


#include <unistd.h>
//real free function(?)
extern void __libc_free(void *ptr);

//Doesn't seem to influence the output
const int init_hook_skips = 0;

int hook_call_id = 0;

void free(void *ptr){
  
  
  static int recursion_detected = 0;
  
  int use_hook = !recursion_detected &&
    hook_call_id >= init_hook_skips;
    
  
  
  if(hook_call_id < init_hook_skips){
    hook_call_id++;
  }
  

  
  if(use_hook){
    recursion_detected = 1;
    printf("simpleFree (%p)(%i)\n", ptr, *(int*)ptr);
    
    struct cudaPointerAttributes attributes;
    
    //cudaPointerGetAttributes needs const pointer.
    //const void * ptr2 = ptr;
    //Seg.faults
    printf("Calling cudaPointerAttributes\n");
    
    
    //Okay now it returns Invalid argument for some reason
    cudaError_t code = cudaPointerGetAttributes(&attributes, ptr);
    
    //If pointer is not registerd with cuda (regualar pointer allocated with malloc)
    //cuda will return invalidValue
    //For now we interpret this as a sign that the pointer was allocated with regualr malloc
    //can lead to problems if an undefined or garbage pointer is passed
    if(code == cudaErrorInvalidValue){
      printf("Freeing regular Pointer \n");
      __libc_free(ptr);
      recursion_detected = 0;
      //we could try to register the pointer here with cuda using cudaHostRegister
      //But I don't see the point if we will free it anyways
    }else if (code == cudaSuccess){
      //If pointer is menaged we can free it with cudaFree
      if (attributes.isManaged){
        cudaFree(ptr);
        printf("Freeing cuda pointer");
        recursion_detected = 0;

      //Freeing unmaged pointer that cuda knows of
      }else{
	printf("Freeing unmaged pointer that cuda knows of");
	__libc_free(ptr);
	recursion_detected = 0;
      }
    }else{
      printf("CudaError: %s \n", cudaGetErrorString(code));
      //exit(-1);
      printf("Not exiting the process for debuging ");
    }
    
    printf("We have passed cuda\n");

    
    
    
  }else{
    __libc_free(ptr);
  }
}
