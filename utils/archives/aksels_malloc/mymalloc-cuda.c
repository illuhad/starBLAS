#include <cuda_runtime_api.h>
#include <stdio.h>


#include <unistd.h>

// This is the original malloc defined in libc (the GNU C standard
// library). Possibly won't work with non-gcc compilers?
extern void *__libc_malloc(size_t size);

// It turns out that the C standard library will call malloc() before
// calling main(). If we call cudaMallocManaged() in this case, the program
// crashes or hangs (because it was partially initialized?)
const int initialization_hook_skips = 1 ;
// We count the call id of malloc(). This is only important
// for skipping pre-initialization mallocs. Since there is
// only one thread before main() is called, we don't need
// to worry about multi-threading here.
int hook_call_id = 0;

void* malloc(size_t size){
  
    // This static variable is a marker whether we are
    // in a recursive malloc() call [cudaMallocManaged() may
    // call malloc() again, which can lead to an infinite loop].
    // Hence, when we set this to 1 before calling cudaMallocManaged()
    // and to 0 again afterwards.
    // [Reminder: This works, because static variables in functions retain
    // their value across different invocations of the function]
    // ToDo: In multi-threading scenarios, this variable could be
    // subject to race-conditions, but let's worry about locks later.
    static int recursion_detected = 0;
    
    // We use the hook if we are not in a malloc recursion and
    // and have skipped the specified number of initialization mallocs
    int use_hook = !recursion_detected &&
      hook_call_id >= initialization_hook_skips;
  
    // Don't unconditionally increment the call id,
    // probably not necessary, but this prevents overflows
    if(hook_call_id < initialization_hook_skips)
      ++hook_call_id;
      
    if(use_hook)
    {
      // Don't call cudaMallocManaged() in recursive malloc() calls
      // before we have exited this block
      recursion_detected = 1;
     
      // printf() may also call malloc(), so don't use before setting
      // the recursion_detected variable
      printf("Hook called!\n");
      void *a;

      cudaError_t code =  cudaMallocManaged(&a, size, cudaMemAttachGlobal);
      if(code != cudaSuccess){
          printf("CUDA error: %s\n", cudaGetErrorString(code));
          exit(-1);
      }
      printf("Exiting hook!\n");
      
      // Also super important, this guarantees that the next malloc() calls
      // can again use unified memory
      recursion_detected = 0;
      
      return a;
    }
    else
    {
      // We don't want to use the hook, so let's just call the regular malloc()
      // from libc.
      
      // Don't (!!!) use dlsym() to obtain the regular malloc() as can be seen 
      // in some solutions on the internet, as dlsym() may call calloc() which may call
      // malloc -> infinite loop
      return __libc_malloc(size);
  
    }
      
}

void free(void* ptr){
  // Here, we also need
  // * recursion detection, since cudaFree() could call free()
  // * initialization skips
  // * before calling cudaFree(), we should check
  // if ptr is actually a cuda pointer - it could be
  // that it was allocated regularly with __libc_malloc()
  // in malloc(). This can be done with cudaPointerGetAttributes().
  // (have a look at the isManaged attribute)
}


