
#ifndef HTABLE_VALUE_TYPE
#error HTABLE_VALUE_TYPE undefined
#endif

#ifndef HTABLE_NAME
#error HTABLE_NAME undefined
#endif

#ifndef HTABLE_KEY_TYPE
#error HTABLE_KEY_TYPE undefined
#endif

#ifndef HTABLE_HASH_FUNCTION
#error HTABLE_HASH_FUNCTION undefined - should be set to a function with signature unsigned long long (HTABLE_KEY_TYPE)
#endif

#define HTABLE_CONCAT_IDENTIFIER_IMPL(a,b) a ## _ ## b
#define HTABLE_CONCAT_IDENTIFIER(a,b) HTABLE_CONCAT_IDENTIFIER_IMPL(a,b)
#define HTABLE_IDENTIFIER(identifier) HTABLE_CONCAT_IDENTIFIER(HTABLE_NAME, identifier)

#ifndef HTABLE_MALLOC
#define HTABLE_MALLOC malloc
#endif

#ifndef HTABLE_FREE
#define HTABLE_FREE free
#endif

#ifndef HTABLE_MAX_LOAD_FACTOR
#define HTABLE_MAX_LOAD_FACTOR 0.7
#endif

#ifndef HTABLE_MIN_LOAD_FACTOR
#define HTABLE_MIN_LOAD_FACTOR 0.1
#endif

#ifndef HTABLE_INITIAL_NUM_BUCKETS
// You can customize this when including htable.h,
// but make sure that it is a power of 2. Otherwise,
// it is not guaranteed that the hash table will correctly
// find and insert elements.
#define HTABLE_INITIAL_NUM_BUCKETS 16
#endif

typedef unsigned long long HTABLE_IDENTIFIER(hash_type);

size_t HTABLE_IDENTIFIER(next_candidate_entry) (HTABLE_IDENTIFIER(hash_type) hash,
                                                size_t current_candidate,
                                                size_t iteration_index,
                                                size_t table_size)
{
  return (size_t)(((double)hash 
                   + 0.5 * iteration_index 
                   + 0.5 * iteration_index*iteration_index)) % table_size;
}

typedef struct
{
  HTABLE_KEY_TYPE key;
  HTABLE_VALUE_TYPE val;
  HTABLE_IDENTIFIER(hash_type) hash;
  int is_used;
  size_t hash_count;
} HTABLE_IDENTIFIER(entry);

typedef struct
{
  size_t num_elements;
  size_t htable_size;
  HTABLE_IDENTIFIER(entry)* entries;
} HTABLE_IDENTIFIER(object);

//HTABLE_IDENTIFIER(object) HTABLE_IDENTIFIER(global_object) = {.hobject_size=0, .entries = NULL};

void HTABLE_IDENTIFIER(init) (HTABLE_IDENTIFIER(object)* ctx)
{
  ctx->entries = 
    HTABLE_MALLOC(sizeof(HTABLE_IDENTIFIER(entry)) * HTABLE_INITIAL_NUM_BUCKETS);
  ctx->htable_size = HTABLE_INITIAL_NUM_BUCKETS;
  ctx->num_elements = 0;
  
  for(size_t i = 0; i < ctx->htable_size; ++i)
  {
    ctx->entries[i].hash_count = 0;
    ctx->entries[i].is_used = 0;
  }
}

void HTABLE_IDENTIFIER(free) (HTABLE_IDENTIFIER(object)* ctx)
{
  HTABLE_FREE(ctx->entries);
  ctx->entries = NULL;
  ctx->htable_size = 0;
}

size_t HTABLE_IDENTIFIER(count) (HTABLE_IDENTIFIER(object)* ctx)
{
  return ctx->num_elements;
}

int HTABLE_IDENTIFIER(_detail_find) (HTABLE_IDENTIFIER(entry)* entries,
                                     HTABLE_KEY_TYPE key,
                                     HTABLE_IDENTIFIER(hash_type) hash,
                                     size_t table_size,
                                     HTABLE_IDENTIFIER(entry)** retrieved_entry)
{
  if(table_size != 0){
    size_t candidate_entry = hash % table_size;
    int hash_count = entries[candidate_entry].hash_count;
    size_t hashing_index = 0;
    for (int investigated_collisions = 0; investigated_collisions < hash_count;)
    {
      if(entries[candidate_entry].is_used)
      {
        if(entries[candidate_entry].key == key)
        {
          *retrieved_entry = &(entries[candidate_entry]);
          return 1;
        }
        if(entries[candidate_entry].hash % table_size ==
          hash % table_size)
          ++investigated_collisions;
      }

      ++hashing_index;
      candidate_entry = HTABLE_IDENTIFIER(next_candidate_entry)(hash,
                                                                candidate_entry,
                                                                hashing_index,
                                                                table_size);
    }
  }
  return 0;
}

int HTABLE_IDENTIFIER(find_with_hash)(HTABLE_IDENTIFIER(object)* ctx,
                                      HTABLE_KEY_TYPE key,
                                      HTABLE_IDENTIFIER(hash_type) hash, 
                                      HTABLE_VALUE_TYPE** retrieved_value)
{
  *retrieved_value = NULL;
  
  HTABLE_IDENTIFIER(entry)* result_entry;
  
  if(HTABLE_IDENTIFIER(_detail_find)(ctx->entries,
                                     key, hash,
                                     ctx->htable_size,
                                     &result_entry))
  {
    *retrieved_value = &(result_entry->val);
    return 1;
  }
  return 0;
}

int HTABLE_IDENTIFIER(find) (HTABLE_IDENTIFIER(object)* ctx, 
                             HTABLE_KEY_TYPE key,
                             HTABLE_VALUE_TYPE** retrieved_value)
{
  HTABLE_IDENTIFIER(hash_type) hash = HTABLE_HASH_FUNCTION(key);
  return HTABLE_IDENTIFIER(find_with_hash) (ctx, key, hash, retrieved_value);
}

void HTABLE_IDENTIFIER(_detail_insert) (HTABLE_IDENTIFIER(entry)* entries,
                                        size_t table_size,
                                        HTABLE_IDENTIFIER(hash_type) hash,
                                        HTABLE_KEY_TYPE key,
                                        HTABLE_VALUE_TYPE value)
{
  size_t candidate_entry = hash % table_size;
  size_t primary_hash_entry = candidate_entry;
  
  size_t hashing_index = 0;
  for(;;)
  {
    if(!entries[candidate_entry].is_used)
    {
      entries[candidate_entry].key = key;
      entries[candidate_entry].val = value;
      entries[candidate_entry].hash = hash;
      entries[candidate_entry].is_used = 1;
      entries[primary_hash_entry].hash_count++;
      
      return;
    }
    ++hashing_index;
    candidate_entry = HTABLE_IDENTIFIER(next_candidate_entry)(hash,
                                                              candidate_entry,
                                                              hashing_index,
                                                              table_size);
  }
}

int HTABLE_IDENTIFIER(_detail_erase) (HTABLE_IDENTIFIER(entry)* entries,
                                      size_t table_size,
                                      HTABLE_KEY_TYPE key,
                                      HTABLE_IDENTIFIER(hash_type) hash)
{
  HTABLE_IDENTIFIER(entry)* result_entry;
  
  if(HTABLE_IDENTIFIER(_detail_find)(entries,
                                     key, hash,
                                     table_size,
                                     &result_entry))
  {
    result_entry->is_used = 0;
    entries[hash % table_size].hash_count--;
    return 1;
  }
  return 0;
}
                                        

void HTABLE_IDENTIFIER(_detail_resize_table) (HTABLE_IDENTIFIER(object)* ctx,
                                              size_t new_size)
{
  HTABLE_IDENTIFIER(entry)* new_entries = 
      HTABLE_MALLOC(sizeof(HTABLE_IDENTIFIER(entry)) * new_size);
      
  for(size_t i = 0; i < new_size; ++i)
  {
    new_entries[i].is_used = 0;
    new_entries[i].hash_count = 0;
  }
      
  for(size_t i = 0; i < ctx->htable_size; ++i)
  {
    if(ctx->entries[i].is_used)
      HTABLE_IDENTIFIER(_detail_insert)(new_entries,
                                        new_size,
                                        ctx->entries[i].hash,
                                        ctx->entries[i].key,
                                        ctx->entries[i].val);
  }
  
  HTABLE_FREE(ctx->entries);
  
  ctx->entries = new_entries;
  ctx->htable_size = new_size;
}

double HTABLE_IDENTIFIER(get_load_factor) (HTABLE_IDENTIFIER(object)* ctx)
{
  return (double)(ctx->num_elements) / (double)(ctx->htable_size);
}

void HTABLE_IDENTIFIER(insert)(HTABLE_IDENTIFIER(object)* ctx,
                               HTABLE_KEY_TYPE key,
                               HTABLE_VALUE_TYPE value)
{
  double load_factor = HTABLE_IDENTIFIER(get_load_factor)(ctx);
  
  if(load_factor > HTABLE_MAX_LOAD_FACTOR)
    HTABLE_IDENTIFIER(_detail_resize_table)(ctx, 2 * ctx->htable_size);
  
  HTABLE_IDENTIFIER(_detail_insert)(ctx->entries, 
                                    ctx->htable_size, 
                                    HTABLE_HASH_FUNCTION(key), 
                                    key, 
                                    value);
  
  ctx->num_elements++;
}

int HTABLE_IDENTIFIER(erase)(HTABLE_IDENTIFIER(object)* ctx,
                             HTABLE_KEY_TYPE key)
{
  
  if(!HTABLE_IDENTIFIER(_detail_erase)(ctx->entries,
                                       ctx->htable_size,
                                       key,
                                       HTABLE_HASH_FUNCTION(key)))
    return 0;
  
  ctx->num_elements--;
  
  double load_factor = HTABLE_IDENTIFIER(get_load_factor)(ctx);
  
  if((load_factor < HTABLE_MIN_LOAD_FACTOR) && 
     (ctx->htable_size > HTABLE_INITIAL_NUM_BUCKETS))
  {
    size_t half_table_size = ctx->htable_size / 2;
    
    size_t new_size = (half_table_size > HTABLE_INITIAL_NUM_BUCKETS) ?
      half_table_size : HTABLE_INITIAL_NUM_BUCKETS;
      
    HTABLE_IDENTIFIER(_detail_resize_table)(ctx, new_size);
  }
  
  return 1;
  
}

int HTABLE_IDENTIFIER(empty) (HTABLE_IDENTIFIER(object)* ctx)
{
    if(ctx->num_elements > 0){
        return 0;
    }else{
        return 1;
    }
}

int HTABLE_IDENTIFIER(pop) (HTABLE_IDENTIFIER(object)* ctx,
                             HTABLE_KEY_TYPE** retrieved_value)
{

    for(size_t i = 0; i < ctx->htable_size; ++i)
    {
        if(ctx->entries[i].is_used){
            printf("%i\n", ctx->entries[i].is_used);
            *retrieved_value = &(ctx->entries[i].key);
            ctx->num_elements--;
            HTABLE_IDENTIFIER(erase)(ctx, ctx->entries[i].key);
            return 1;
        }

    }

    return 0;
}

#undef HTABLE_HASH_FUNCTION
#undef HTABLE_INITIAL_NUM_BUCKETS
#undef HTABLE_MIN_LOAD_FACTOR
#undef HTABLE_MAX_LOAD_FACTOR
#undef HTABLE_IDENTIFIER
#undef HTABLE_KEY_TYPE
#undef HTABLE_VALUE_TYPE
#undef HTABLE_NAME
