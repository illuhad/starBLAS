
#include <stdio.h>
#include <stdlib.h>

unsigned long long float_hash(float x)
{ 
  return (unsigned long long)(1.e5f * x);
}

// Define table with float keys and integer
// values
#define HTABLE_KEY_TYPE float
#define HTABLE_VALUE_TYPE int
// The table name - this name must be unique
// within the source file.
#define HTABLE_NAME float_table
// The hash function that should be used for hashing.
// Must return unsigned long long and have one parameter
// of type HTABLE_KEY_TYPE.
#define HTABLE_HASH_FUNCTION float_hash
// Including htable.h generates a hash table type according to
// the above definitions. The table type is named
// HTABLE_NAME_object (here, float_table_object) and
// all functions are called HTABLE_NAME_function_name.
// For example, float_table_init().
// By defining HTABLE_MALLOC and HTABLE_FREE, you can also
// specify which malloc/free functions the table should use :-)
#include "htable.h"
// The htable.h file "consumes" the above definitions, so they
// are not available here anymore.

unsigned long long int_hash(int x)
{ 
  return (unsigned long long)(2*x);
}

// We can also define other table types, as long
// as their name is unique. Here we define a hash table
// that maps integers to other integers.
#define HTABLE_KEY_TYPE int
#define HTABLE_VALUE_TYPE int
#define HTABLE_NAME int_table
#define HTABLE_HASH_FUNCTION int_hash
#include "htable.h"

const int test_size = 100;

int main()
{
  // Declare hash table object
  float_table_object float_tbl;
  // Initialize hash table object. Must (!!)
  // be called before any operations on the table
  // take place.
  // First parameter of all hash table functions is
  // always a pointer to the object on which they should operate
  float_table_init(&float_tbl);
  
  // First we insert a couple of key-value pairs into
  // the table. The table will grow (and shrink) automatically
  // as needed when elements are inserted/removed.
  for(int i = 0; i < test_size; ++i)
  {
    // To make testing with many keys easier, we just
    // calculate them from our loop variable - in practice
    // this is of course not particulary useful ;)
    float key = 0.5f * (float)i;
    int value = i;
    printf("Mapping %f -> %d\n",key,value);
    
    float_table_insert(&float_tbl, key, value);
  }
  
  // Look up elements that exist and 10 elements
  // that don't
  for(int i = 0; i < test_size+10; ++i)
  {
    float key = 0.5f * (float)i;
    int* query_result;
    // The find function returns 1 if the key was found,
    // 0 otherwise. The last argument is a pointer-to-pointer
    // to a HTABLE_VALUE_TYPE variable. If the key was found,
    // the function will write the address of the value belonging
    // to the key into the location specified by the argument.
    // This approach also allows to modify the value stored in
    // the hash table, if needed.
    if(float_table_find(&float_tbl, key, &query_result))
      printf("Found value for %f: %d\n", key, *query_result);
    else
      printf("No entry for key %f\n", key);
  }
  
  // Let's remove all elements except for every 10th.
  for(int i = test_size - 1; i >= 0; --i)
  {
    if(i % 10 != 0)
    {
      float key = 0.5f * (float)i;
      // The erase function returns 1 if the specified key was found (and erased),
      // and 0 otherwise.
      if(float_table_erase(&float_tbl, key))
        printf("Erased %f\n",key);
      else printf("Could not erase %f.\n");
    }
  }
  
  // Print again what's left in the table
  for(int i = 0; i < test_size; ++i)
  {
    float key = 0.5f * (float)i;
    int* query_result;
    if(float_table_find(&float_tbl, key, &query_result))
      printf("Found mapping %f -> %d\n", key, *query_result);
    else
      printf("No entry for element %f\n", key);
  }
  
  printf("%lu elements are remaining in the hash table.\n",
         float_table_count(&float_tbl));
  
  // Release memory at the end
  float_table_free(&float_tbl);
}

