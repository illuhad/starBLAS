#include"hash_table.h"
#include"timer.hpp"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void bench_insert(int times){
  ht_hash_table* ht = ht_new();
  
  cumulative_timer timer;
  for(int i = 0; i < times; i++){
     timer.start();
  
     void* key = (void*)rand();
     int value = rand();
     
     ht_insert(ht, key, value);
     timer.stop();
  }
  printf("%f,", timer.get_average_runtime());
  ht_del_hash_table(ht);
}

void bench_find(int size, int acces){
  // Creating hash_table with size:%i\n"
  
  ht_hash_table* ht = ht_new();
  void* test[size];
  
  for(int i = 0; i < size; i++){
     void* key = (void*)rand();
     int value = rand();
     ht_insert(ht, key, value);
     test[i] = key;
  }
  
  //Accessing access times
  cumulative_timer timer;
  for(int i = 0; i < size; i++){
     timer.start();
     void* key = test[rand()%size];
     int out;
     int a = ht_search(ht, &out, key);
     timer.stop();
     if (a!= 1){
       printf("Error");
    }
  }
  
  printf("%f,", timer.get_total_runtime());
  
  ht_del_hash_table(ht);
}

int main(){
  printf("Inserting 5000-150000 elements\n");
  printf("numInserts,1,2,3,4,5,\n");
  for(int times = 1; times < 30; times++){
    printf("%i,", times*5000);
    for(int i = 0; i < 5; i++){
      bench_insert(times*5000);
    }
    printf("\n"); 
  }
  printf("*************ACCES-TEST**************\n");
  printf("Accessing 10000 times table size 5k-150k\n");
  printf("sizeTable,1,2,3,4,5,\n");
  for(int times = 1; times < 300; times++){
    printf("%i,", times*50000);
    for(int i = 0; i < 5; i++){
      bench_find(10000, times*10000);
    }
    printf("\n"); 
  }
  /*
  void* p = malloc(sizeof(int));
  for(int i=0; i < 100; i++){
    ht_insert(ht,i, i*3);
   // printf("inserted: %i \n", i);
    if (ht_search(ht,p, i)){
      printf("k:v %i:%i\n", i, *(int*)p);
    }
  }
  t = clock()-t;
  double time_taken = ((double)t)/CLOCKS_PER_SEC;
  printf("inserting took %f seconds to execute \n", time_taken); 
  //printf("%i", ht_search(ht,32));
  //ht_del_hash_table(ht);
  */
  //printf("random acces of 1000 element\n");
 
}

