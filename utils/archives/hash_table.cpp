#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdint.h>
#include "hash_table.h"
#include "prime.h"

#ifdef TEST
#define MALLOC malloc
#define FREE free
#define CALLOC calloc
#else
#define MALLOC __libc_malloc
#define FREE __libc_free
#define CALLOC __libc_calloc

extern void *__libc_malloc(size_t size);
extern void __libc_free(void* size);
extern void *__libc_calloc(size_t num, size_t size);

#endif

//Deleting can break a collision chain, making items at the end impossibl acces
//sentinell item 
//Shuldn't these be in the .h file??
static ht_item HT_DELETED_ITEM = {NULL, NULL};
const static int HT_INITIAL_BASE_SIZE = 60;
int HT_PRIME_1; 
int HT_PRIME_2;

static ht_item* ht_new_item(const void* k, const int v) {
  ht_item* i = (ht_item*)MALLOC(sizeof(ht_item));
  i->key = k;
  i->value = v;
  return i;
}

//Creats new hash table with new size
static ht_hash_table* ht_new_sized(const int base_size) {
    ht_hash_table* ht = (ht_hash_table*)MALLOC(sizeof(ht_hash_table));
    ht->base_size = base_size;
    ht->size = next_prime(ht->base_size);
    HT_PRIME_1 = ht->size;
    HT_PRIME_2 = next_prime(HT_PRIME_1+1);
    ht->count = 0;
    ht->items = (ht_item**)CALLOC((size_t)ht->size, sizeof(ht_item*));
    return ht;
}

ht_hash_table* ht_new(){
  return ht_new_sized(HT_INITIAL_BASE_SIZE);
}



static void ht_del_item(ht_item* i) {
  FREE(i);
}

void ht_del_hash_table(ht_hash_table* ht){
  for (int i = 0; i < ht->size; i++){
    ht_item* item = ht->items[i];
    if(item != NULL){
      if(item != &HT_DELETED_ITEM){
        ht_del_item(item);
      }
    }
  }
  FREE(ht->items);
  FREE(ht);
}

static void* ht_hash(const void* input, int prime, int bucket_length){
  uintptr_t out = (uintptr_t)input % prime;
  return (void *)out;
}

//Implementing with double hashing as for now
//ToDo: If necessery update to linear hashing
static int ht_get_hash(const void* input, const int num_buckets, const int attempt){
  const void* hash_a = ht_hash(input, HT_PRIME_1, num_buckets);
  const void* hash_b = ht_hash(input, HT_PRIME_2, num_buckets);
  //return (int)(((uintptr_t)hash_a + (attempt * ((uintptr_t)hash_b + 1))) % num_buckets);
  return (int)((uintptr_t)hash_b + attempt)%num_buckets;
}


static void ht_resize(ht_hash_table* ht, const int base_size) {
    //we can't have a hash table smaller than the base size
    if (base_size < HT_INITIAL_BASE_SIZE) {
        return;
    }
ht_hash_table* new_ht = ht_new_sized(base_size);
    for (int i = 0; i < ht->size; i++) {
        ht_item* item = ht->items[i];
        if (item != 0x0 && item != &HT_DELETED_ITEM) {
            ht_insert(new_ht, item->key, item->value);
        }
    }

    ht->base_size = new_ht->base_size;
    ht->count = new_ht->count;

    const int tmp_size = ht->size;
    ht->size = new_ht->size;
    new_ht->size = tmp_size;

    ht_item** tmp_items = ht->items;
    ht->items = new_ht->items;
    new_ht->items = tmp_items;

    ht_del_hash_table(new_ht);  
   }


static void ht_resize_up(ht_hash_table* ht) {
    const int new_size = ht->base_size * 2;
    ht_resize(ht, new_size);
}


static void ht_resize_down(ht_hash_table* ht) {
    const int new_size = ht->base_size / 2;
    ht_resize(ht, new_size);
}



//Inserting
int ht_insert(ht_hash_table* ht, const void* key, const int value){
  const int load = ht->count * 100 / ht->size;
    if (load > 70) {
        ht_resize_up(ht);
    }
  ht_item* item = ht_new_item(key, value);
  int  index = ht_get_hash(item->key, ht->size, 0);
  ht_item* cur_item = ht->items[index];
  int i = 1;
  //change || to && iff not working
  while (cur_item != 0x0 && cur_item != &HT_DELETED_ITEM){
    index = ht_get_hash(item->key, ht->size, i);
    cur_item = ht->items[index];
    i++;
  }
  ht->items[index] = item;
  ht->count++;
  return 1;
}

//searching

int ht_search(ht_hash_table* ht, int* out, const void* key) {
    uintptr_t index = (uintptr_t)ht_get_hash(key, ht->size, 0);
    ht_item* item = ht->items[index];
    int i = 1;
    while (item != NULL) {
      if (item != &HT_DELETED_ITEM){
        if (item->key == key) {
            *out = item->value;
	    return 1;
        }
        index = ht_get_hash(key, ht->size, i);
        item = ht->items[index];
        i++;

      }else if (item == &HT_DELETED_ITEM){
	index = ht_get_hash(key, ht->size, i);
        item = ht->items[index];
	i++;
      }
    } 
    return 0;
}


int ht_delete(ht_hash_table* ht, const void* key) {
    int succes = -1;
    const int load = ht->count * 100 / ht->size;
    if (load < 10) {
        ht_resize_down(ht);
    }

    int index = ht_get_hash(key, ht->size, 0);
    ht_item* item = ht->items[index];
    int i = 1;
    while (item != NULL) {
        if (item != &HT_DELETED_ITEM) {
            if (item->key == key) {
                ht_del_item(item);
                ht->items[index] = &HT_DELETED_ITEM;
                succes = 1;
	      
	    }
        }
        index = ht_get_hash(key, ht->size, i);
        item = ht->items[index];
        i++;
    } 
    ht->count--;
    return succes;
}



