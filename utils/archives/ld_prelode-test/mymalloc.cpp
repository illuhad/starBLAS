#include <iostream>
#include <dlfcn.h>




void* malloc(std::size_t size){
  auto real_malloc = dlsym(RTLD_NEXT, "malloc");
  if (NULL == real_malloc) {
     std::cout << "dlsym error" << std::endl;
  }

   void *p = NULL;
   std::cout << "malloc" << size << std::endl;
   p = real_malloc(size);
   std::cout << p << std::endl;
   return p;
}
