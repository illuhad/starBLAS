
#ifdef __cplusplus

extern "C" {
#endif
typedef struct {
  const void* key;
  int value;
} ht_item;

typedef struct {
  int size;
  int count;
  ht_item** items;
  int base_size;
} ht_hash_table;


//For debuging
ht_hash_table* ht_new();
void ht_del_hash_table(ht_hash_table* ht);

//Api
int ht_insert(ht_hash_table* ht, const void* key, const int value);
int ht_search(ht_hash_table* ht, int* out, const void* key);
int ht_delete(ht_hash_table* ht , const void* key);
#ifdef __cplusplus
}
#endif
