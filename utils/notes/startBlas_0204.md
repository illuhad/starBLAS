# idea Further work
async calls to a library, checks if the sequenz of calls worth going to GPU or not 

# finishing the starBLAS library
## After profiling the following code parts are slowing down the computation (generating CPU page faults)
* HPL_dlocmax XXX
* HPL_pdmaxswp
* HPL_dlocswpT XXX
* HPL_dlatcpy XXX
* HPL_dlaswp00N in lesser extent
Can't preload these functions unles they are undefined. explained [here](https://stackoverflow.com/questions/38745799/how-to-make-static-linked-elf-file-to-load-ld-preload-so)

If I am right these functions can be very easely ported to cuda kernels. Here is the plan: 
~~ 
1. Remove the files from the HPL dirctory
2. Write a library that has these as cuda kernels 
~~
1. Replace the functions with cuda function calls
2. compile HPL with nvcc
3. Preload this library with the hook
4. Awsomeness



