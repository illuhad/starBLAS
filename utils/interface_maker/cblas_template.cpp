#include<cblas_core.h>

void gpu_<function_family_name>(cublasHandle_t handle,
             <GPU_function_var_name_type>){
    cublasStatus_t stat;
    stat = cublasD<function_family_name>(handle, <GPU_function_var_name>);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_<function_family_name>( const std::string& name,
                 <CBLAS_funciton_var_name_type_templated>){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_<function_family_name>)(<CBLAS_funciton_var_name_type_templated>);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_<function_family_name>,
                //Host
                [&](){
                real_cblas_<function_family_name>(<CBLAS_vars>);
                },
                //Device
                [&](){
                gpu_<function_family_name>(handle, <GPU_vars_with_cast>);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void <function_name>(<CBLAS_funciton_var_name_type>){
    cblas_<function_family_name>("<function_name>", <CBLAS_vars>);
}
}

