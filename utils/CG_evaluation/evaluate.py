import os
import csv
import statistics
from datetime import datetime

class TestSuit:
    def __init__(self, name, iterator):
        self.iterator = iterator 
        self.name = name
        self.tests = []
        self.init = "/bin/bash module load compiler/gcc/7.3.0 core/cuda/10.0 core/openmpi/3.1.4 core/Openblas/0.3.6"
        self.numIter = 2
        self.dir = "../results/{}_{}".format(self.name, datetime.now()).replace(" ", "_")[:-7]
        self.data_file = self.dir + "/{}".format(self.name)
        

    def addTest(self, test):
        self.tests.append(test)
        
    def runSuit(self):
        for test in self.tests:
            for i in self.iterator:
                test.execute(i, self.numIter)

    def writeResults(self):
        os.mkdir(self.dir)
        with open(self.data_file, "w+") as f:
            writer = csv.writer(f)
            writer.writerow(["test name"] + list(self.iterator))
            for test in self.tests:
                writer.writerow([test.name] + test.getResultsAvg())
                writer.writerow([test.name + "_Std"] + test.getResultsStd())

class Test:
    def __init__(self, cmd_line, name, testSuit, init_cmd=None):
        self.cmd_line = cmd_line
        self.results = {}
        self.name = name
        self.suit = testSuit
        self.init_cmd = init_cmd
        testSuit.addTest(self)

    def __repr__(self):
        return self.results

    def __str__(self):
        return str(self.results)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.results[key]
        elif isinstance(key, slice):
            return self.results[key]
        else:
            raise TypeError('Index must be int, not {}'.format(type(key).__name__))            

    def execute(self, size, numIter):
        self.results[size] = []
        if self.init_cmd != None:
            os.system(self.init_cmd)
            print(self.init_cmd)
        for i in range(numIter):
            cmd = "./bash_exec \'{}\' \'{}\' ".format(self.init_cmd, self.cmd_line.format(size, 1))
            print(cmd)
            os.system(cmd)
            with open("temp.out") as a:
                time = float(a.readline().strip('\n').replace("AVG: ", ""))
                print(time)
                self.results[size].append(time)

    def getResultsAvg(self):
        return [sum(i)/len(i) for i in self.results.values()]

    def getResultsStd(self):
        return [statistics.stdev(i) for i in self.results.values()]


testSuit = TestSuit("intel_compiler_proper_source_quick", range(1000, 10000, 1000))


Test("OMP_PLACES=sockets OMP_PROC_BIND=1  OMP_NUM_THREADS=40 ./conjugate_gradient {} 1 2> /dev/null | grep AVG  > temp.out", "Oblas_SocketProcBind", testSuit, init_cmd="ml load compiler/gcc/7.3.0 core/cuda/10.0 core/openmpi/3.1.4 core/Openblas/0.3.6 core/cuda/10.0")

Test(" OMP_PROC_BIND=true  OMP_NUM_THREADS=40 ./conjugate_gradient {} 1 2> /dev/null | grep AVG  > temp.out", "Oblas_SocketProcBind_true", testSuit, init_cmd="ml load compiler/gcc/7.3.0 core/cuda/10.0 core/openmpi/3.1.4 core/Openblas/0.3.6 core/cuda/10.0")


Test("OMP_PROC_BIND=close  OMP_NUM_THREADS=40 ./conjugate_gradient {} 1 2> /dev/null | grep AVG  > temp.out", "Oblas_ProcBindClose", testSuit, init_cmd="ml load compiler/gcc/7.3.0 core/cuda/10.0 core/openmpi/3.1.4 core/Openblas/0.3.6 core/cuda/10.0")

Test("LD_PRELOAD=./libhook.so ./conjugate_gradient {} 1 2> /dev/null | grep AVG  > temp.out", "Starblas", testSuit, init_cmd="ml load compiler/gcc/7.3.0 core/cuda/10.0 core/openmpi/3.1.4 core/Openblas/0.3.6 core/cuda/10.0")

Test("./conjugate_gradient_mkl {} 1 2> /dev/null | grep AVG  > temp.out", "MKL", testSuit, init_cmd="ml load core/cuda/10.0 core/openmpi/3.1.4 core/Openblas/0.3.6 compiler/icc/2019.4.243 core/mkl/4.0.1" )


testSuit_intel = TestSuit("intel_compiler_proper_source_quick", range(1000, 10000, 1000))


Test("OMP_PLACES=sockets OMP_PROC_BIND=1  OMP_NUM_THREADS=40 ./conjugate_gradient {} 1 2> /dev/null | grep AVG  > temp.out", "Oblas_SocketProcBind", testSuit_intel, init_cmd="ml load core/cuda/10.0 core/openmpi/3.1.4 core/Openblas/0.3.6 core/cuda/10.0")

testSuit_intel.runSuit()
testSuit_intel.writeResults()

