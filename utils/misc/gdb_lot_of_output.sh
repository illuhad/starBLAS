#!/bin/bash
cd ../../build/
sh ./compile.sh
gdb ./test
echo -e "set env LD_PRELOAD=./libhhok.so"
echo "set env LD_LIBRARY_PATH=/usr/local/cuda-9.2/lib64"
echo "run"
