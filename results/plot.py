#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 22:35:55 2019

@author: soproni
"""

import matplotlib.pyplot as plt
import numpy as np
raw_data = np.loadtxt("result-mkl-oblas-starblas.csv", dtype='str',delimiter=",")
print(raw_data[4][0])
data = {}

for j in range(len(raw_data)):
    curr = raw_data[j]
    data[raw_data[j][0]] = []
    for i in range(1, len(raw_data[j]), 1):
        data[raw_data[j][0]].append(float(raw_data[j][i]))
print(data)

width = 0.35
ind = np.arange(len(data["test name"]))*2
#ind = [i+(width/2) for i in ind]
print(ind)
ind_loc = ind + width/2

fig, ax = plt.subplots()
rects = []

rects.append(ax.bar(ind_loc, data[list(data.keys())[1]], width, yerr=data[list(data.keys())[2]],
                label=list(data.keys())[1]))

rects.append(ax.bar(ind_loc + width, data[list(data.keys())[3]], width, yerr=data[list(data.keys())[4]],
                label=list(data.keys())[3]))

rects.append(ax.bar(ind_loc + width*3, data[list(data.keys())[5]], width, yerr=data[list(data.keys())[6]],
                label=list(data.keys())[5]))

rects.append(ax.bar(ind_loc + width*2, data[list(data.keys())[7]], width, yerr=data[list(data.keys())[8]],
                label=list(data.keys())[7]))

ax.set_ylabel('Runtime (s)')
ax.set_xlabel('Matrix number of rows')
ax.set_title('Performance comperison of the starBLAS')
ax.grid(True)
ax.set_xticks(ind + width*2)
ax.set_xticklabels(tuple([ int(i) for i in data["test name"]]))
ax.legend()

def autolabel(rects, xpos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0, 'right': 1, 'left': -1}

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(offset[xpos]*3, 3),  # use 3 points offset
                    textcoords="offset points",  # in both directions
                    ha=ha[xpos], va='bottom')


#autolabel(rects[0])
#autolabel(rects[1])
#autolabel(rects[2])
#autolabel(rects[3])


fig.tight_layout()

plt.show()