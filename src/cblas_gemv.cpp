#include<cblas_core.h>

void gpu_gemv(cublasHandle_t handle,
             cublasOperation_t TransA, const int M, const int N, const double alpha, const double* A, const int lda, const double* X, const int incX, const double beta, double* Y, const int incY){
    cublasStatus_t stat;
    stat = cublasDgemv(handle, TransA, M, N, &alpha, A, lda, X, incX, &beta, Y, incY);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_gemv( const std::string& name,
                 const enum CBLAS_ORDER order, const enum CBLAS_TRANSPOSE TransA, const int M, const int N, const double alpha, const T* A, const int lda, const T* X, const int incX, const double beta,  T* Y, const int incY){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_gemv)(const enum CBLAS_ORDER order, const enum CBLAS_TRANSPOSE TransA, const int M, const int N, const double alpha, const T* A, const int lda, const T* X, const int incX, const double beta,  T* Y, const int incY);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_gemv,
                //Host
                [&](){
                real_cblas_gemv(order, TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY);
                },
                //Device
                [&](){
                gpu_gemv(handle, getOperation(TransA), M, N, alpha, A, lda, X, incX, beta, Y, incY);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_dgemv(const enum CBLAS_ORDER order, const enum CBLAS_TRANSPOSE TransA, const int M, const int N, const double alpha, const double* A, const int lda, const double* X, const int incX, const double beta, double* Y, const int incY){
    //printf("Gemv is called");
    cblas_gemv("cblas_dgemv", order, TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY);
}
}

