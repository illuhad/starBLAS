#include <cstddef>
#include <atomic>
#include <thread>
#include <unordered_map>
#include <cassert>
#include <mutex>
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <cuda_runtime_api.h>
#include <new> // bad_alloc, bad_array_new_length
#include <link.h>
#include <dlfcn.h>

extern "C" {

void *__libc_malloc(size_t len);
void *__libc_realloc(void *ptr, size_t len);
void __libc_free(void *ptr);

}
// Anonymous namespace to hide symbols from
// other parts of the application
namespace
{

// Allocator class that can be used in C++ STL containers to
// force allocation through libc
template <class T>
struct libc_allocator
{
    using value_type = T;
    libc_allocator() noexcept = default;

    template <class U>
    libc_allocator(const libc_allocator<U> &) noexcept {}

    template <class U>
    bool operator==(const libc_allocator<U> &) const noexcept { return true; }

    template <class U>
    bool operator!=(const libc_allocator<U> &) const noexcept { return false; }

    T *allocate(const size_t len) const
    {
        if (len == 0)
            return nullptr;

        void *ptr = __libc_malloc(len * sizeof(T));

        if (!ptr)
            throw std::bad_alloc();

        return static_cast<T *>(ptr);
    }

    void deallocate(T *ptr, size_t) const noexcept
    {
        __libc_free(ptr);
    }
};

template <class Key, class Value>
using nohook_unordered_map = 
    std::unordered_map<Key, Value, std::hash<Key>, 
                       std::equal_to<Key>, 
                       libc_allocator<std::pair<Key, Value>>>;

template<class T>
using nohook_basic_string =
    std::basic_string<
        T, 
        std::char_traits<T>,
        libc_allocator<T>>;

using nohook_string = nohook_basic_string<char>;

template<class T>
using nohook_vector =
    std::vector<T, libc_allocator<T>>;


class scoped_flag_disable
{
public:
    scoped_flag_disable(bool& flag)
    : _old_value{flag},_flag{flag} 
    {
        _flag = false;
    }

    ~scoped_flag_disable()
    {
        _flag = _old_value;
    }

private:
  bool _old_value;
  bool &_flag;
};

struct address_range
{
    void* begin;
    void* end;
};

class library_address_space_resolver
{
public:
    library_address_space_resolver(
        const nohook_vector<nohook_string>& library_names)
        : _library_names{library_names}
    {
        dl_iterate_phdr(iterate_callback, reinterpret_cast<void*>(this));
    }

    bool contains_address(void* addr) const
    {
        for(const auto& range : _ranges)
            if(addr >= range.begin && addr < range.end)
                return true;
        return false;
    }

    // In addition to contains_address, this will also check
    // dladdr() for the library from which the symbol comes.
    // This can also catch libraries that are loaded dynamically
    // during the runtime of the program!
    // Note that dladdr() uses malloc(), so this function should only
    // be used if libdl is part of the blocked libraries.
    bool contains_dynamic_address(void* addr)
    {
        if(contains_address(addr))
            return true;

        Dl_info info;
        int err = dladdr(addr, &info);
        assert(err != 0);

        nohook_string filename = info.dli_fname;

        for (const nohook_string &library_name : _library_names)
            if(filename.find(library_name) != std::string::npos)
                return true;

        return false;
    }

private:

    static int iterate_callback(dl_phdr_info* info, size_t size, void* data)
    {
        library_address_space_resolver* this_ptr = 
            reinterpret_cast<library_address_space_resolver*>(data);

        nohook_string library_name = info->dlpi_name;
        std::cout << "... program uses library: " << library_name << std::endl;

        // Check if name of so file matches any of the libraries we are interested in
        for(const auto& name : this_ptr->_library_names)
        {
            if(library_name.find(name) != std::string::npos)
            {
                std::cout << "   [investigating]" << std::endl;
                // We have one of the libraries we were looking for!
                // Walk all its segments and store the virtual memory ranges
                for(int segment_index = 0; segment_index < info->dlpi_phnum;
                    ++segment_index)
                {
                    char* start_addr = reinterpret_cast<char*>(info->dlpi_addr) 
                                     + info->dlpi_phdr[segment_index].p_vaddr;
                    char* end_addr = start_addr + info->dlpi_phdr[segment_index].p_memsz;

                    this_ptr->_ranges.push_back({reinterpret_cast<void*>(start_addr), 
                                                 reinterpret_cast<void*>(end_addr)});

                    std::cout << " --> mapped to range: "
                            << this_ptr->_ranges.back().begin << " - "
                            << this_ptr->_ranges.back().end << std::endl;
                }
            }
        }

        return 0;
    }

    const nohook_vector<nohook_string> _library_names;
    nohook_vector<address_range> _ranges;
};


class hook
{
  public:
    hook()
    : _blocked_address_spaces{
        nohook_vector<nohook_string>{
            /*"cuda",
            "libc.so",
            "libstdc++",
            "pthread",
            "libdl.so",
            "ld-linux-x86-64.so",
            "libmpi.so",
            "openmpi",
            "libnuma"*/
            //"cuda",
            ".so"
        }},
        _device_shutdown{false}
    {
        enable();
    }

    void enable()
    {
        // Additional lock to guarantee that
        // no other thread is allowed to change the
        // hook status while another thread holds the
        // lock
        lock_guard lock{_lock};
        _is_hook_enabled = true;
    }

    void disable()
    {
        lock_guard lock{_lock};
        _is_hook_enabled = false;
    }

    /// Once called, only regular allocations/frees will be processed.
    /// All other will be ignored.
    void device_shutdown()
    {
        lock_guard lock{_lock};
        this->_device_shutdown = true;

        if(_allocation_map.empty())
            std::cout << "device shutdown: <no open unified memory allocations, exiting cleanly>"
                      << std::endl;

        for(auto entry : _allocation_map)
        {
            std::cout << "device shutdown: lost region of unified memory: "
                      << entry.second << " bytes @ "
                      << entry.first << std::endl;
        }

        if(!_allocation_map.empty())
            std::cout << "device shutdown: Remaining unified memory " 
                      << "allocations may now be inaccessible, undefined behavior on access is expected."
                      << std::endl;
    }

    void* malloc(void* caller, size_t len)
    {  
        if(_is_hook_enabled)
        {
            if(_blocked_address_spaces.contains_dynamic_address(caller))
                return __libc_malloc(len);

            // Since we are using a recursive mutex, we can lock right at the beginning
            // No other threads will be able to interfere with us modifying the _is_hook_enabled flag!
            lock_guard lock{_lock};

            void *ptr = nullptr;

            if(!_device_shutdown)
            {
                scoped_flag_disable disable_hook{_is_hook_enabled};

                ptr = device_allocate(len);

                _allocation_map[ptr] = len;

                return ptr;
            }
            else
            {
                std::cout << "Warning: malloc(): not using device allocation, "
                                "only regular allocations are allowed in this context"
                            << std::endl;
            }
        }
        
        return __libc_malloc(len);
    }

    bool is_managed_memory(void* ptr) const
    {
        lock_guard lock{_lock};
        scoped_flag_disable disable_hook{_is_hook_enabled};

        return this->_allocation_map.find(ptr) != _allocation_map.end();
    }

    void free(void* caller, void* ptr)
    {
        if(_blocked_address_spaces.contains_dynamic_address(caller))
        {
            __libc_free(ptr);
            return;
        }

        bool is_managed = this->is_managed_memory(ptr);

        if(is_managed)
        {
            lock_guard lock{_lock};
            if(!_device_shutdown)
            {
                scoped_flag_disable disable_hook{_is_hook_enabled};

                auto it = _allocation_map.find(ptr);
                assert(it != _allocation_map.end());
                _allocation_map.erase(it);

                cudaFree(ptr);
            }
            else
            {
                std::cout << "Warning: free(): Ignoring request to free "
                          << "device memory since device is unavailable. A device memory leak is expected."
                          << std::endl;
            }
        }
        else
            __libc_free(ptr);
    }

    void* realloc(void* caller, void *ptr, size_t size)
    {
        if(_blocked_address_spaces.contains_dynamic_address(caller))
            return __libc_realloc(ptr, size);

        lock_guard lock{_lock};

        bool is_managed = is_managed_memory(ptr);

        if(is_managed)
        {
            std::size_t old_size = 0;

            void *new_ptr = nullptr;


            if(!_device_shutdown)
            {
                scoped_flag_disable disable_hook{_is_hook_enabled};


                assert(_allocation_map.find(ptr) != _allocation_map.end());
                old_size = _allocation_map[ptr];

                new_ptr = device_allocate(size);
            }
            else
            {
                std::cout << "Warning: realloc(): Not reallocating on "
                                "device since device is unavailable. A device memory leak is expected."
                            << std::endl;
                new_ptr = __libc_malloc(size);
            }
            std::copy(reinterpret_cast<char *>(ptr),
                    reinterpret_cast<char *>(ptr) + std::min(old_size, size),
                    reinterpret_cast<char *>(new_ptr));

            if(!_device_shutdown)
            {
                scoped_flag_disable disable_hook{_is_hook_enabled};
                cudaFree(ptr);

                _allocation_map.erase(ptr);
                _allocation_map[new_ptr] = size;
            }
    

            return new_ptr;
        }
        else
            return __libc_realloc(ptr, size);
    }

    std::size_t get_total_allocated_device_memory() const
    {
        return _total_device_memory_allocations.load();
    }

  private:
    void* device_allocate(std::size_t len)
    {
        void *new_ptr = nullptr;
        cudaError_t err = cudaMallocManaged(&new_ptr, len, cudaMemAttachGlobal);
        assert(err == cudaSuccess);

        _total_device_memory_allocations += len;
        return new_ptr;
    }

    library_address_space_resolver _blocked_address_spaces;

    using allocation_map_type = nohook_unordered_map<void*, std::size_t>;

    allocation_map_type _allocation_map;

    // We use a recursive mutex to avoid deadlocks when being called
    // again from CUDA
    mutable std::recursive_mutex _lock;
    using lock_guard = std::lock_guard<std::recursive_mutex>;

    mutable bool _is_hook_enabled;
    bool _device_shutdown;

    std::atomic<std::size_t> _total_device_memory_allocations;
};

bool is_hook_ready = false;
// Scott-Meyers singleton for the hook instance.
class hook_manager
{
public:
    static bool is_ready() { return is_hook_ready; }

    static hook& get() { return get_manager()._hook; }

    // The object will be constructed when this function
    // is called the first time.
    // In order to activate the hook, it is necessary to call it once
    // eplicitly!
    static hook_manager &get_manager() 
    {
        static hook_manager manager;
        return manager;
    }

    static void start()
    {
        get_manager();
        is_hook_ready = true;
    }
private:

    hook _hook;
};

} // anonymous namespace

// Actual hooks are here
extern "C" {

void *malloc(size_t len)
{ 
    void* caller = __builtin_return_address(0);
    if(hook_manager::is_ready())
        return hook_manager::get().malloc(caller, len); 
    else
        return __libc_malloc(len);
}

void free(void *ptr)
{
    void* caller = __builtin_return_address(0);
    if(hook_manager::is_ready())
        hook_manager::get().free(caller, ptr); 
    else
        __libc_free(ptr);
}

void* realloc(void* ptr, size_t len)
{
    void* caller = __builtin_return_address(0);
    if(hook_manager::is_ready())
        return hook_manager::get().realloc(caller, ptr, len);
    else
        return __libc_realloc(ptr, len);
}

typedef int(*mpi_init_function_type)(int*,char***);
typedef int(*mpi_finalize_function_type)();

int MPI_Init(int *argc, char ***argv)
{
    mpi_init_function_type original_mpi_init = nullptr;
    original_mpi_init = 
        reinterpret_cast<mpi_init_function_type>(dlsym(RTLD_NEXT, "MPI_Init"));

    int return_value = -1;

    if(original_mpi_init != nullptr)
        return_value = original_mpi_init(argc, argv);
    else
        std::cout << "Warning: could not resolve symbol MPI_Init()." << std::endl;

    hook_manager::start();

    return return_value;
}

int MPI_Finalize()
{
    mpi_finalize_function_type original_mpi_finalize = nullptr;
    original_mpi_finalize =
        reinterpret_cast<mpi_finalize_function_type>(dlsym(RTLD_NEXT, "MPI_Finalize"));

    assert(hook_manager::is_ready());
    std::size_t total_allocated_memory = 
        hook_manager::get().get_total_allocated_device_memory();

    hook_manager::get().device_shutdown();

    int return_value = -1;
    if(original_mpi_finalize != nullptr)
        return_value = original_mpi_finalize();
    else
        std::cout << "Warning: could not resolve symbol MPI_Finalize()." << std::endl;

    std::cout << "======== Total allocated unified memory over runtime ======="
              << std::endl
              << "               " << total_allocated_memory << " bytes" << std::endl;

    return return_value;
}
}

