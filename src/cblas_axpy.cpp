#include<cblas_core.h>

void gpu_axpy(cublasHandle_t handle,
             const int N, const double alpha, const double* X, const int incX, double* Y, const int incY){
    cublasStatus_t stat;
    stat = cublasDaxpy(handle, N, &alpha, X, incX, Y, incY);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << "cuBLAS error" << std::endl;
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_axpy( const std::string& name,
                 const int N, const double alpha, const T* X, const int incX,  T* Y, const int incY){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_axpy)(const int N, const double alpha, const T* X, const int incX,  T* Y, const int incY);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_axpy,
                //Host
                [&](){
                real_cblas_axpy(N, alpha, X, incX, Y, incY);
                },
                //Device
                [&](){
                gpu_axpy(handle, N, alpha, X, incX, Y, incY);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_daxpy(const int N, const double alpha, const double* X, const int incX, double* Y, const int incY){
    cblas_axpy("cblas_daxpy", N, alpha, X, incX, Y, incY);
}
}

