#include<cblas_core.h>

void gpu_ger(cublasHandle_t handle,
             const int M, const int N, const double alpha, const double* X, const int incX, const double* Y, const int incY, double* A, const int lda){
    cublasStatus_t stat;
    stat = cublasDger(handle, M, N, &alpha, X, incX, Y, incY, A, lda);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_ger( const std::string& name,
                 const enum CBLAS_ORDER order, const int M, const int N, const double alpha, const T* X, const int incX, const T* Y, const int incY,  T* A, const int lda){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_ger)(const enum CBLAS_ORDER order, const int M, const int N, const double alpha, const T* X, const int incX, const T* Y, const int incY,  T* A, const int lda);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_ger,
                //Host
                [&](){
                real_cblas_ger(order, M, N, alpha, X, incX, Y, incY, A, lda);
                },
                //Device
                [&](){
                gpu_ger(handle, M, N, alpha, X, incX, Y, incY, A, lda);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_dger(const enum CBLAS_ORDER order, const int M, const int N, const double alpha, const double* X, const int incX, const double* Y, const int incY, double* A, const int lda){
    cblas_ger("cblas_dger", order, M, N, alpha, X, incX, Y, incY, A, lda);
}
}

