#include<cblas_core.h>

void gpu_swap(cublasHandle_t handle,
             const int N, double* X, const int incX, double* Y, const int incY){
    cublasStatus_t stat;
    stat = cublasDswap(handle, N, X, incX, Y, incY);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_swap( const std::string& name,
                 const int N,  T* X, const int incX,  T* Y, const int incY){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_swap)(const int N,  T* X, const int incX,  T* Y, const int incY);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_swap,
                //Host
                [&](){
                real_cblas_swap(N, X, incX, Y, incY);
                },
                //Device
                [&](){
                gpu_swap(handle, N, X, incX, Y, incY);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_dswap(const int N, double* X, const int incX, double* Y, const int incY){
    cblas_swap("cblas_dswap", N, X, incX, Y, incY);
}
}

