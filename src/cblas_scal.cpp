#include<cblas_core.h>

void gpu_scal(cublasHandle_t handle,
             const int N, const double alpha, double* X, const int incX){
    cublasStatus_t stat;
    stat = cublasDscal(handle, N, &alpha, X, incX);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_scal( const std::string& name,
                 const int N, const double alpha,  T* X, const int incX){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_scal)(const int N, const double alpha,  T* X, const int incX);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_scal,
                //Host
                [&](){
                real_cblas_scal(N, alpha, X, incX);
                },
                //Device
                [&](){
                gpu_scal(handle, N, alpha, X, incX);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_dscal(const int N, const double alpha, double* X, const int incX){
    cblas_scal("cblas_dscal", N, alpha, X, incX);
}
}

