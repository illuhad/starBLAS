#include<cblas_core.h>

void gpu_gemm(cublasHandle_t handle,
              cublasOperation_t transA,
              cublasOperation_t transB,
              const int m,
              const int n,
              const int k,
              const double alpha,
              const double* dA,
              const int ldda,
              const double* dB,
              const int lddb,
              const double beta,
              double* dC,
              const int lddc){
    cublasStatus_t stat;
    stat = cublasDgemm(handle, transA,
                  transB,
                  m, n, k, &alpha, dA, ldda,
                  dB, lddb, &beta, dC, lddc);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_gemm( const std::string& name,
                 const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB,
                 const int M, const int N,const int K,
                 const T alpha, const T *A,
                 const int lda, const T *B,
                 const int ldb,const T beta,
                 T *C, const int ldc){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_gemm)(const enum CBLAS_ORDER,
                                     const enum CBLAS_TRANSPOSE,
                                     const enum CBLAS_TRANSPOSE,
                                     const int,
                                     const int,
                                     const int,
                                     const T,
                                     const T*,
                                     const int,
                                     const T*,
                                     const int,
                                     const T,
                                     T*,
                                     const int);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_gemm,
                //Host
                [&](){
                real_cblas_gemm(Order, TransA, TransB,
                                 M, N, K, alpha, A, lda,
                                 B, ldb, beta, C, ldc);
                },
                //Device
                [&](){
                gpu_gemm(handle, getOperation(TransA),
                               getOperation(TransB),
                              M, N, K, alpha, A, lda,
                              B, ldb, beta, C, ldc);
                },
                {
                make_readwrite_buffer(C, M),
                make_readwrite_buffer(A, N),
                make_readwrite_buffer(B, K),
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_dgemm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB,
                 const int M, const int N,const int K,
                 const double alpha, const double *A,
                 const int lda, const double *B,
                 const int ldb,const double beta,
                 double *C, const int ldc){
    //printf("asdf");
    cblas_gemm("cblas_dgemm",
               Order, TransA, TransB,
               M, N, K, alpha, A, lda,
               B, ldb, beta, C, ldc);
}
}
