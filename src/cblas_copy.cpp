#include<cblas_core.h>

void gpu_copy(cublasHandle_t handle,
             const int N, const double* X, const int incX, double* Y, const int incY){
    cublasStatus_t stat;
    stat = cublasDcopy(handle, N, X, incX, Y, incY);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;

    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_copy( const std::string& name,
                 const int N, const T* X, const int incX,  T* Y, const int incY){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_copy)(const int N, const T* X, const int incX,  T* Y, const int incY);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_copy,
                //Host
                [&](){
                real_cblas_copy(N, X, incX, Y, incY);
                },
                //Device
                [&](){
                gpu_copy(handle, N, X, incX, Y, incY);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_dcopy(const int N, const double* X, const int incX, double* Y, const int incY){
    cblas_copy("cblas_dcopy", N, X, incX, Y, incY);
}
}

