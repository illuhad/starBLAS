#include<cblas_core.h>

int gpu_max(cublasHandle_t handle,
             const int N, const double* X, const int incX){
    cublasStatus_t stat;
    int result;
    stat = cublasIdamax(handle, N, X, incX, &result);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
    return result;
}

template<class T>
int cblas_max( const std::string& name,
                 const int N, const T* X, const int incX){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static int  (*real_cblas_max)(const int N, const T* X, const int incX);


    int returnV;
    int returnV_CPU;
    returnV = execute_blas_int(
                name,
                //Host fucntion ptr type
                real_cblas_max,
                //Host
                [&](int * o){
                *o = real_cblas_max(N, X, incX);
                },
                //Device
                [&](int * o){
                // Cublas uses 1-based indexing used for compatibility with Fortran.
                // but HPL expects 0-based indexing, therfore, we should increment
                // the stride with one (N)
                // and decrement the result
                *o = gpu_max(handle, N+1, X, incX) - 1;
                },
                {
                },
                static_cast<size_t>(2*N));

      return returnV;
}




extern "C"{
int cblas_idamax(const int N, const double* X, const int incX){
    int g;
    g=cblas_max("cblas_idamax", N, X, incX);

    //std::cout << g << std::endl;
    return g;
}
}
