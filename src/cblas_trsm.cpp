#include<cblas_core.h>

void gpu_trsm(cublasHandle_t handle,
             cublasSideMode_t Side, cublasFillMode_t Uplo, cublasOperation_t TransA, cublasDiagType_t Diag, const int M, const int N, const double alpha, const double* A, const int lda, double* B, const int ldb){
    cublasStatus_t stat;
    stat = cublasDtrsm(handle, Side, Uplo, TransA, Diag, M, N, &alpha, A, lda, B, ldb);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_trsm( const std::string& name,
                 const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side, const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag, const int M, const int N, const double alpha, const T* A, const int lda,  T* B, const int ldb){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_trsm)(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side, const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag, const int M, const int N, const double alpha, const T* A, const int lda,  T* B, const int ldb);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_trsm,
                //Host
                [&](){
                real_cblas_trsm(Order, Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb);
                },
                //Device
                [&](){
                gpu_trsm(handle, getSide(Side), getFill(Uplo), getOperation(TransA), getDiag(Diag), M, N, alpha, A, lda, B, ldb);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_dtrsm(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side, const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag, const int M, const int N, const double alpha, const double* A, const int lda, double* B, const int ldb){
    cblas_trsm("cblas_dtrsm", Order, Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb);
}
}

