#include <cblas_core.h>

bool should_run_on_gpu(const std::vector<blas_buffer>& arrays, size_t num_operations){
    for(const blas_buffer &a : arrays){
        //std::cout << "checking pointer: " << a.ptr << std::endl;
        cudaError_t stat;
        cudaPointerAttributes pointAtt;
        stat = cudaPointerGetAttributes(&pointAtt, a.ptr);
        if (stat != cudaSuccess){
            printf("Cehecking pointer (%p)(%i)\n", a.ptr, *(int*)a.ptr);
            printf("CUDA error (should run on gpu): %s\n", cudaGetErrorString(stat));
            exit(-1);
        }
        if(!pointAtt.type == cudaMemoryTypeManaged){
            std::cout << "not device" << std::endl;
            std::cout  << &a << std::endl;

            return false;
        }
    }
    //std::cout << "Device" << std::endl;
    return true;

}

cublasFillMode_t getFill(CBLAS_UPLO a){
    if (a == 121){
        return CUBLAS_FILL_MODE_UPPER;
    }else if (a == 122){
        return CUBLAS_FILL_MODE_LOWER;
    }
    throw "invalid input";
}


cublasDiagType_t getDiag(CBLAS_DIAG a){
    if (a == 131){
        return CUBLAS_DIAG_NON_UNIT;
    } else if (a == 132){
        return CUBLAS_DIAG_UNIT;
    }
    throw "invalid input";
}

cublasSideMode_t getSide(CBLAS_SIDE a){
    if(a == 141){
        return CUBLAS_SIDE_LEFT;
    }else if(a == 142){
        return CUBLAS_SIDE_RIGHT;
    }
    throw "invalid input";
}


cublasOperation_t getOperation(CBLAS_TRANSPOSE a){
    if(a==111){
        return CUBLAS_OP_N;
    }else if (a==112){
        return  CUBLAS_OP_T;
    }else if (a==113){
        return  CUBLAS_OP_C;
    }else{
    throw "invalid input";
    }
}
