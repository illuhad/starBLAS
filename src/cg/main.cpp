/*
Conjugent Gradient solver

This program serves as a benchmark for the starBLAS library
*/


#include <cblas.h>
#include <iostream>
#include <cstring>
#include <mpi.h>
#include <omp.h>
#include "timer.hpp"

/// dcopy function wrapper for better readibility
void dcopy(double * A, double * B, int rows){
    cblas_dcopy(rows, A, 1 , B, 1);
}

void print( char  name, const double* matrix, int row, int column)
{
  printf("Matrix has %d rows and %d columns:\n",  row, column);
  for (int i = 0; i < row; i++){
    for (int j = 0; j < column; j++){
      printf("%.5f ", matrix[j*row + i]);
    }
    printf("\n");
  }
  printf("\n");
}

/// Generates symetric Positve definite matricies
void init_problem(double* matrix, double* b, int row, int column)
{
    double *C = reinterpret_cast<double*>(malloc(sizeof(double) * row * row));
    double a = 0;
    #pragma omp parallel for 
    for (int j = 0; j < column; j++){
        for (int i = 0; i < row; i++){
            a = (double)rand()/RAND_MAX;
            //std::cout << a << " " << i << ";" << j << std::endl;

            matrix[j*row + i] = a;
            C[j*row + i] = 0;
       //     matrix[i*row + j] = a;
        }
        C[j] = 0;
        b[j] = 1;
    }
    CBLAS_ORDER order = CblasColMajor;
    CBLAS_TRANSPOSE noTrans = CblasNoTrans;
    CBLAS_TRANSPOSE Trans = CblasTrans;

    // generating symetric matrix (M*M')
    cblas_dgemm(order, noTrans, Trans, row, row, row, 1, matrix, row, matrix, row, 1, C, row);
    std::copy(C, C+row*row, matrix);
}

void init_example_problem(double* matrix, double* b, int row, int column){
    b[0] = 1; b[1] = 2;
    matrix[0] = 4; matrix[1] = 1;
    matrix[2] = 1; matrix[3] = 3;
}


void solve_cg_from_python(const double  *A, const double  *b, int const rowsA, double *x0)
{
    CBLAS_ORDER order = CblasColMajor;
    CBLAS_TRANSPOSE noTrans = CblasNoTrans;


    double *rk = reinterpret_cast<double*>(malloc(sizeof(double) * rowsA));
    double *pk = reinterpret_cast<double*>(malloc(sizeof(double) * rowsA));
    double *Ap = reinterpret_cast<double*>(malloc(sizeof(double) * rowsA));


    double alpha;
    double r_kplus1_norm;
    double beta;


    //Our initial guess
    for (int i = 0; i < rowsA; i++){
        x0[i] = 1;
    }

    // here rk is b, but we couldn't use b in a blas call because b is const
    std::copy(b, b+rowsA, rk);

    // rk = Multiply(A, x) - b we compute the difference from the solution
    cblas_dgemv(order, noTrans,rowsA, rowsA, 1.0, A, rowsA, x0, 1.0, -1.0, rk, 1.0);

    // pk = - r p is basicall the direction in which we should go, therefore we
    // go in the oposite direction than our vector rk points.
    cblas_dcopy(rowsA, rk, 1, pk, 1);
    cblas_dscal(rowsA, -1, pk, 1);

    //Simple counter so we now how many iterations it took to compute x
    int k = 0;


    double rk_norm = cblas_ddot(rowsA, rk, 1, rk, 1);

    // We zero out the Ap vector.
    std::memset(Ap, 0, rowsA*sizeof(*Ap));


    while(true){

        // computing nominator for alpha_k

        //std::memset(Ap, 0, rowsA*sizeof(*Ap));
        // vector * vector Ap*p
        cblas_dgemv(order, noTrans, rowsA, rowsA, 1, A, rowsA, pk, 1, 0, Ap, 1);
        alpha = rk_norm / cblas_ddot(rowsA, pk, 1, Ap, 1);

        // Computing x_k+1
        // x0+= alpha*pk
        cblas_daxpy(rowsA, alpha, pk, 1, x0, 1);

        // Computing r_k+1
        // r += alpha*Ap
        cblas_daxpy(rowsA, alpha, Ap, 1, rk, 1);

        r_kplus1_norm = cblas_ddot(rowsA, rk, 1, rk, 1);

        // Computing beta_k
        beta = r_kplus1_norm / rk_norm;

        rk_norm = r_kplus1_norm;

        if (r_kplus1_norm < 1.0e-10){
            std::cout << "Finished in : " << k << " iterations " << std::endl;
            free(rk);
            free(pk);
            free(Ap);
            break;
        }
        // compute pk_plus1
        // beta*pk - r
        cblas_dscal(rowsA, beta, pk, 1);
        cblas_daxpy(rowsA, -1, rk, 1, pk, 1);

        // Increment itration counter
        k++;

    }
}

void execute_iter(int size, cumulative_timer& Timer){
    CBLAS_ORDER order = CblasColMajor;
    CBLAS_TRANSPOSE noTrans = CblasNoTrans;



    int rowsA = size;
    double *A = reinterpret_cast<double*>(malloc(sizeof(double) * rowsA * rowsA));
    double *b = reinterpret_cast<double*>(malloc(sizeof(double) * rowsA));
    double *x0 = reinterpret_cast<double*>(malloc(sizeof(double) * rowsA));
    init_problem(A, b, rowsA, rowsA);
    //init_example_problem(A, b, 2, 2);

    Timer.start();
    solve_cg_from_python(A, b, rowsA, x0);
    Timer.stop();

    std::cout << "checking solution..." << std::endl;
    cblas_dgemv(order, noTrans, rowsA, rowsA, 1.0, A, rowsA, x0, 1, -1, b, 1);
    std::cout << "maximal difference is:" << b[cblas_idamax(rowsA, b, 1)] << std::endl;



}

//arg1 size arg2 epoch size
int main(int argc, char *argv[])
{
    MPI_Init(NULL, NULL);
    int size, epoch;
    if (argc == 3){
        size = atoi(argv[argc-2]);
        epoch = atoi(argv[argc-1]);
    }else if (argc == 2){
        size = atoi(argv[argc-1]);
        epoch = 1;
    } else {
        std::cout << "Please specify the size of the matrix (arg1) "
                  << "and the number of itterations (arg2)" << std::endl;
        exit(-1);
    }

    cumulative_timer Timer;

    for (int i =0; i < epoch; i++){
        execute_iter(size, Timer);
    }
    std::cout << "AVG: " << Timer.get_average_runtime() << std::endl;

    MPI_Finalize();

}
