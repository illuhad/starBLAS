#include <cuda_runtime_api.h>
#include <cuda_profiler_api.h>
#include <iostream>
#include <pthread.h>
#include <dlfcn.h>
#include <cstring>
#include <mutex>
#include <cublas_v2.h>
#include <cstdlib>
#include <execinfo.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "queue.hpp"
#include "address_space_resolver.hpp"


extern "C" {
extern void *memcpy(void *str1, const void *str2, size_t n);
}

///Prints Backtrace
void print_backtrace(void)
{
    static const char start[] = "BACKTRACE ------------\n";
    static const char end[] = "----------------------\n";

    void *bt[1024];
    int bt_size;
    char **bt_syms;
    int i;

    bt_size = backtrace(bt, 1024);
    bt_syms = backtrace_symbols(bt, bt_size);
    for (i = 1; i < bt_size; i++) {
        size_t len = strlen(bt_syms[i]);
        std::cout << bt_syms[i] << std::endl;
    }
    free(bt_syms);
}


/*!
 \brief singleton class for the malloc, realloc, free and memcopy hooks

 It will be only activeted if MPI_INIT have already been called
 The hook wont be called for the libraries that are listed in the library_names vector
 (This should be configurable in a later production version)
 Reason for excluding some libraries is that they might will be called by malloc and when malloc is preloaded
 and therefore if malloc is called inside our_malloc function than it will result in a infinte recursive funciton call
*/
class Hook
{
private:
    std::recursive_mutex rec_mutex; ///< Recursive mutex for ensuring thread safety

    //! libraries that will call the regular malloc
    /*!
      These libraries has been provent to call malloc
      or in some other way interfere with the hook mechanism.
      Therefore these libraries are going to get blacklisted
    */
    nohook_vector<nohook_string> library_names {"libcuda.so","libcuinj64","cublas", "pthread",
                                                "nvidia","blas", "libc", "linux-vdso", "libgcc",
                                                "libstd", "cuda", "dl", "sysv", "libcudart"};
    //! Hashtable for storing the pointer values
    /*!
      We use a hastable for saving the pointers that we have
      allocated. This hashtable uses our custom malloc for allocation
      therfore it is safe to use inside our own malloc implementation.

      After allocating a memory with cudaMemoryManged the returned pointer
      is stored in the hastable as a <pointer, size> pair. We need the size
      information for memcopy, and realloc later.

      If free is called a hashtable lookup will determine wether the memory has been allocated
      with cudaMallocManageed and therfore needs to be freed up with cudaFree or it was regulary
      allocated and can be freed up with regular free
    */
    nohook_unordered_map<void*, std::size_t> hashtable;

    library_address_space_resolver resolver = library_address_space_resolver(library_names); ///< Library adress space resolver



    Hook(){

    }

public:
    static Hook& get(){
        //static varibles initalized at the first time a function is called
        static Hook h;
        return h;
    }

    ///Our malloc hook. Implements the regular malloc interface
    void* malloc(size_t size){
        // We use the hook if we are not in a recursion and malloc is not comming from a blacklisted library
        if(!resolver.contains_dynamic_address(__builtin_return_address(1))){

            

            void *a;
            cudaError_t code =  cudaMallocManaged(&a, size, cudaMemAttachGlobal);


            if(code != cudaSuccess){
                //Debug output in case of a faliure
                printf("CUDA error (malloc): %s\n", cudaGetErrorString(code));
                print_backtrace();
                std::cout << size << std::endl;
                std::cout << "Allocating with __libc_malloc" << std::endl;
                a = __libc_malloc(size);
                std::cout << "Allocated address:" << a << std::endl;
                std::cout << "size:" << size << std::endl;
                std::cout << "Return address: " << __builtin_return_address(0) << std::endl;
                std::cout << "Library walk return: " << resolver.contains_dynamic_address(__builtin_return_address(1)) << std::endl;

                return a;

                exit(-1);
            }
            // we might want to use cudaMemAdvise in later releases

            /*
             cudaMemAdvise(a, size, cudaMemAdviseSetReadMostly, 0);
             cudaMemAdvise(a, size, cudaMemAdviseSetPreferredLocation, 0);
             cudaMemAdvise(a, size, cudaMemAdviseSetAccessedBy, cudaCpuDeviceId);
            */
            rec_mutex.lock();
            hashtable.insert({a, size});

            // this guarantees that the next malloc() calls
            // can again use unified memory
            rec_mutex.unlock();
            return a;
        }
        else
        {
            // We don't want to use the hook, so let's just call the regular malloc()
            // from libc.
            return __libc_malloc(size);

        }
    }

    /// Free Hook implements regular free interface
    void free(void *ptr){

        //recursion_detection as seen in malloc

        // Also this part should be described sufficently above

        /*
        we will use the hook if we are not in a recurson, and no cuda function was called the malloc
        hook
        */
        if(resolver.contains_dynamic_address(__builtin_return_address(0))){
            if( (hashtable.find(ptr) != hashtable.end())){
                rec_mutex.lock();
                //Activate recursion protection


                if(ptr != NULL){
                    cudaError_t code = cudaFree(ptr);
                    if (code != cudaSuccess){
                        printf("simpleFreeERROR (%p)(%i)\n", ptr, *(int*)ptr);
                        printf("CUDA error (free): %s\n", cudaGetErrorString(code));
                        exit(-1);
                    }
                }
                hashtable.erase(ptr);

                rec_mutex.unlock();
            }
            else{

                __libc_free(ptr);
            }
        }
    }

    /// realloc hook implements regular realloc interface
    void* realloc(void *ptr, size_t size){
        //Check if call from blacklisted library
        if(resolver.contains_dynamic_address(__builtin_return_address(0))){
            rec_mutex.lock();
            if(hashtable.find(ptr) != hashtable.end()){
                int value = hashtable[ptr];
                void* a = malloc(size);
                if (size <= value){
                    memcpy(a, ptr, size);
                }else{
                    memcpy(a, ptr, value);
                }
                free(ptr);

                rec_mutex.unlock();
                return a;
            }
        }else{
            return __libc_realloc(ptr, size);
        }
    }

    void free_all_memory(){
        int a = 0;
        for (auto& it: this->hashtable){
            a++;
            cudaFree(it.first);
        }
        std::cout << "freed " << a << " unfreed memory adresses" << std::endl;
    }
};
/// Singelton class for managing (activating & deactivating the hook)
class HookManager{
private:
    bool MPI_Init_called = false;
public:
    HookManager(){}

    bool isHookEnabled(){
        return this->MPI_Init_called;
    }

    void enableHook(){
        Hook::get();
        this->MPI_Init_called = true;
    }

    void disableHook(){
        cudaProfilerStop();
	this->MPI_Init_called = false;
    }
};

//Global variables in order to intialise the HookManager befor main starts
HookManager hookManager;
cublasHandle_t handle;

/// in order to mitigate the use of the hook before the main function starts we will only activate the hook
/// if MPI_Init is called. therfore only memory allocations after the call of MPI_init will be allocated by our hook
///
/// TODO: Add statistic on the memory accesses before and after MPI_init was called. we could coupple this
/// with NVprof output in order to see if this is the problem which is causeing the cpu pagefaulst. however
/// this is not anymore necassary because one can just tell nvvp which source code to look into and than you
/// have line numbers ans stuff available.
extern "C" {
void MPI_Init(void * a, void* b){
    //Loads orignal MPI_Init() funciton
    
    std::cout << "MPI Init Intercepted" << std::endl;
    static void (*real_MPI_Init)(void*, void*);
    real_MPI_Init = reinterpret_cast<void (*)(void*, void*)>(dlsym(RTLD_NEXT, "MPI_Init"));
    real_MPI_Init(a,b);
    cublasStatus_t stat;

    stat = cublasCreate(&handle);
    if (stat != CUBLAS_STATUS_SUCCESS) {
        std::cout << stat << std::endl;
        printf ("CUBLAS initialization failed\n");
        exit(-1);
    }
    cudaProfilerStart();
    hookManager.enableHook();
}
}

extern "C" {

///Finialises the hook at the end of the application
///
/// TODO: free all memory allocated by us & and print statistic
void MPI_Finalize(){
    
    cudaProfilerStop();
    
    std::cout << "***************************" << std::endl;
    std::cout << "Finializing the HOOK" << std::endl;
    // we don't want want the hook to interfere with malloc and hook after main has ended.
    // better disable it here
    hookManager.disableHook();
    cudaDeviceSynchronize();
    cublasDestroy(handle);
    cudaDeviceReset();

    std::cout << "Attempting to free memory left unfreed" << std::endl;
    Hook::get().free_all_memory();


    std::cout << "Calling regular MPI_Finalize" << std::endl;
    static void (*real_MPI_Finalize)();
    real_MPI_Finalize = reinterpret_cast<void (*)()>(dlsym(RTLD_NEXT, "MPI_Finalize"));


    std::cout << "Since the unload order of the libraries can't be determined the application" <<
                 " might crash after this line" << std::endl;
    std::cout << "*******************" << std::endl;
    real_MPI_Finalize();
}
}

void* malloc(size_t size){
    if (hookManager.isHookEnabled()){

        return Hook::get().malloc(size);
    }else{
        return __libc_malloc(size);
    }
}

void free(void *ptr){
    if (hookManager.isHookEnabled()){
        Hook::get().free(ptr);
    }else{
        __libc_free(ptr);
    }
}
void* realloc(void *ptr, size_t size){
    if (hookManager.isHookEnabled()){
        return Hook::get().realloc(ptr, size);
    }else{
        return __libc_realloc(ptr, size);
    }
}



