#include<cblas_core.h>

void gpu_trsv(cublasHandle_t handle,
             cublasFillMode_t Uplo, cublasOperation_t TransA, cublasDiagType_t Diag, const int N, const double* A, const int lda, double* X, const int incX){
    cublasStatus_t stat;
    stat = cublasDtrsv(handle, Uplo, TransA, Diag, N, A, lda, X, incX);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
}

template<class T>
void cblas_trsv( const std::string& name,
                 const enum CBLAS_ORDER order, const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag, const int N, const T* A, const int lda,  T* X, const int incX){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static void  (*real_cblas_trsv)(const enum CBLAS_ORDER order, const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag, const int N, const T* A, const int lda,  T* X, const int incX);



    execute_blas(
                name,
                //Host fucntion ptr type
                real_cblas_trsv,
                //Host
                [&](){
                real_cblas_trsv(order, Uplo, TransA, Diag, N, A, lda, X, incX);
                },
                //Device
                [&](){
                gpu_trsv(handle, getFill(Uplo), getOperation(TransA), getDiag(Diag), N, A, lda, X, incX);
                },
                {
                },
                static_cast<size_t>(2*N));
    }

extern "C"{
void cblas_dtrsv(const enum CBLAS_ORDER order, const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag, const int N, const double* A, const int lda, double* X, const int incX){
    cblas_trsv("cblas_dtrsv", order, Uplo, TransA, Diag, N, A, lda, X, incX);
}
}

