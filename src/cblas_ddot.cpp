#include<cblas_core.h>

int gpu_dot(cublasHandle_t handle,
             const int N, const double* X, const int incX, const double* Y, const int incY){
    cublasStatus_t stat;
    double result;
    stat = cublasDdot_v2(handle, N, X, incX, Y, incY, &result);
    if (stat != CUBLAS_STATUS_SUCCESS){
        std::cout << stat << std::endl;
    }
    cudaDeviceSynchronize();
    return result;
}

template<class T>
int cblas_dot( const std::string& name,
                 const int N, const T* X, const int incX, const T* Y, const int incY){

    ////std::cout << "cblas_dgemm is called From the cblas_file!!"<< std::endl;

    static int  (*real_cblas_dot)(const int N, const T* X, const int incX, const T* Y, const int incY);


    int returnV;
    int returnV_CPU;
    returnV = execute_blas_int(
                name,
                //Host fucntion ptr type
                real_cblas_dot,
                //Host
                [&](int * o){
                *o = real_cblas_dot(N, X, incX, Y, incY);
                },
                //Device
                [&](int * o){
                *o = gpu_dot(handle, N, X, incX, Y, incY);
                },
                {
                },
                static_cast<size_t>(2*N));

      return returnV;
}




extern "C"{
int cblas_ddot(const int N, const double* X, const int incX, const double* Y,  const int incY){
    int g;
    g=cblas_dot("cblas_idamax", N, X, incX, Y, incY);

    //std::cout << g << std::endl;
    return g;
}
}
