#include <new>
#include <iostream>
#include <unordered_map>
#include <vector>

extern "C" {
    extern void __libc_free(void *ptr);

    // This is the original malloc defined in libc (the GNU C standard
    // library). Possibly won't work with non-gcc compilers?
    extern void *__libc_malloc(size_t size);

    extern void *__libc_realloc(void *ptr, size_t size);
}

// Allocator class that can be used in C++ STL containers to
// force allocation through libc
template <class T>
struct libc_allocator
{
    using value_type = T;
    libc_allocator() noexcept = default;

    template <class U>
    libc_allocator(const libc_allocator<U> &) noexcept {}

    template <class U>
    bool operator==(const libc_allocator<U> &) const noexcept { return true; }

    template <class U>
    bool operator!=(const libc_allocator<U> &) const noexcept { return false;}

    T *allocate(const size_t len) const
    {
        if (len == 0)
            return nullptr;

        void *ptr = __libc_malloc(len * sizeof(T));

        if (!ptr)
            throw std::bad_alloc();

        return static_cast<T *>(ptr);
    }

    void deallocate(T *ptr, size_t) const noexcept
    {
        __libc_free(ptr);
    }
};

// Typedefs for new string, vector and unordered_map
// types that rely on the libc_allocator
template <class Key, class Value>
using nohook_unordered_map =
    std::unordered_map<Key, Value, std::hash<Key>,
                       std::equal_to<Key>,
                       libc_allocator<std::pair<const Key, Value>>>;

template<class T>
using nohook_basic_string =
    std::basic_string<
        T,
        std::char_traits<T>,
        libc_allocator<T>>;

using nohook_string = nohook_basic_string<char>;

template<class T>
using nohook_vector =
    std::vector<T, libc_allocator<T>>;
