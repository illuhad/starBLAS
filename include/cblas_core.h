#include <iostream>
#include <dlfcn.h>
#include <vector>
#include <cublas_v2.h>
#include <cuda_runtime_api.h>
#include "queue.hpp"

//Do we need this?
extern "C" {

enum CBLAS_ORDER {CblasRowMajor=101, CblasColMajor=102};
enum CBLAS_TRANSPOSE {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113};
enum CBLAS_UPLO {CblasUpper=121, CblasLower=122};
enum CBLAS_DIAG {CblasNonUnit=131, CblasUnit=132};
enum CBLAS_SIDE {CblasLeft=141, CblasRight=142};
}

// Represents the different possible ways of
// accessing a blas data buffer
enum class buffer_type
{
  read,
  write,
  readwrite
};

// Represents a data range passed to blas
// It consists of the access type, the size of
// the data range and the actual data pointer
struct blas_buffer
{
  buffer_type type;
  size_t size_bytes;
  const void* ptr;
};

// Here are a couple of helper functions to simplify the
// creation of blas_buffer objects
template<class T>
blas_buffer make_buffer(buffer_type type, const T* data, int num_elements)
{
  return blas_buffer{
    type,
    static_cast<size_t>(num_elements*sizeof(T)),
    static_cast<const void*>(data)
  };
}

template<class T>
blas_buffer make_read_buffer(const T* data, int num_elements)
{ return make_buffer(buffer_type::read, data, num_elements); }

template<class T>
blas_buffer make_write_buffer(const T* data, int num_elements)
{ return make_buffer(buffer_type::write, data, num_elements); }

template<class T>
blas_buffer make_readwrite_buffer(const T* data, int num_elements)
{ return make_buffer(buffer_type::readwrite, data, num_elements); }

bool should_run_on_gpu(const std::vector<blas_buffer>& arrays, size_t num_operations);

cublasOperation_t getOperation(CBLAS_TRANSPOSE a);
cublasSideMode_t getSide(CBLAS_SIDE a);
cublasDiagType_t getDiag(CBLAS_DIAG a);
cublasFillMode_t getFill(CBLAS_UPLO a);


//Later use this: https://stackoverflow.com/questions/26876880/in-c-how-do-i-template-the-return-value-to-be-unique-from-the-parameter-value
template<class Function_ptr_type,
         class Host_executor,
         class Device_executor>
void execute_blas(const std::string& function_name,
                  Function_ptr_type& fn,
                  Host_executor run_on_host,
                  Device_executor run_on_device,
                  const std::vector<blas_buffer>& arrays,
                  size_t num_operations)
{
  if(should_run_on_gpu(arrays, num_operations)){
    //std::cout << "On device" << std::endl;
    run_on_device();
  }
  else
  {
    //std::cout << "On host" << std::endl;
    void* function_ptr = dlsym(RTLD_NEXT, function_name.c_str());
    // Set the captured function pointer
    fn = reinterpret_cast<Function_ptr_type>(function_ptr);

    run_on_host();
  }
}

template<class Function_ptr_type,
         class Host_executor,
         class Device_executor>
int execute_blas_int(const std::string& function_name,
                  Function_ptr_type& fn,
                  Host_executor run_on_host,
                  Device_executor run_on_device,
                  const std::vector<blas_buffer>& arrays,
                  size_t num_operations)
{
  int returnV = -1;
  if(should_run_on_gpu(arrays, num_operations)){
    //std::cout << "On device" << std::endl;
    run_on_device(&returnV);
  }
  else
  {
    //std::cout << "On host" << std::endl;
    void* function_ptr = dlsym(RTLD_NEXT, function_name.c_str());
    // Set the captured function pointer
    fn = reinterpret_cast<Function_ptr_type>(function_ptr);

    run_on_host(&returnV);
  }
  return returnV;
}


template<class Function_ptr_type,
         class Host_executor,
         class Device_executor>
double execute_blas_double(const std::string& function_name,
                  Function_ptr_type& fn,
                  Host_executor run_on_host,
                  Device_executor run_on_device,
                  const std::vector<blas_buffer>& arrays,
                  size_t num_operations)
{
  double returnV = -1;
  if(should_run_on_gpu(arrays, num_operations)){
    //std::cout << "On device" << std::endl;
    run_on_device(&returnV);
  }
  else
  {
    //std::cout << "On host" << std::endl;
    void* function_ptr = dlsym(RTLD_NEXT, function_name.c_str());
    // Set the captured function pointer
    fn = reinterpret_cast<Function_ptr_type>(function_ptr);

    run_on_host(&returnV);
  }
  return returnV;
}
