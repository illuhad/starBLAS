#include <dlfcn.h>
#include <link.h>
#include <iostream>
#include <cassert>

#include "nohook_types.hpp"


struct address_range
{
    void* begin;
    void* end;
};

/// Determines address ranges of loaded libraries. Objects of this class
/// must (!) be created before the hook is active because the constructor
/// may call malloc/free.
class library_address_space_resolver
{
public:
    /// Constructs object.
    /// \param library_names A vector of library names that will
    /// be matched against the filesystem paths of the loaded libraries.
    /// A match occurs if the path contains the string from library_names.
    /// For example, the following will match libcuda.so, libcudart.so and
    /// libstdc++.so:
    /// \c library_address_space_resolver resolver(nohook_vector<nohook_string>{"cuda","stdc++"});
    /// while this will match all shared libraries:
    /// \c library_address_space_resolver resolver(nohook_vector<nohook_string>{".so});
    /// The class will then determine and store the virtual memory ranges
    /// belonging to all matched libraries that are currently loaded.
    library_address_space_resolver(
        const nohook_vector<nohook_string>& library_names)
        : _library_names{library_names}
    {
        dl_iterate_phdr(iterate_callback, reinterpret_cast<void*>(this));
    }

    // Checks if an address is part of the virtual address space of
    // the libraries specified in the constructor.
    // It is guaranteed that this will not call malloc or free.
    bool contains_address(void* addr) const
    {
        for(const auto& range : _ranges)
            if(addr >= range.begin && addr < range.end)
                return true;
        return false;
    }

    // In addition to contains_address, this will also check
    // dladdr() for the library from which the symbol comes.
    // This can also catch libraries that are loaded dynamically
    // during the runtime of the program!
    // Note that dladdr() uses malloc(), so this function should only
    // be used if libdl is part of the blocked libraries.
    bool contains_dynamic_address(void* addr)
    {
        if(contains_address(addr))
            return true;

        Dl_info info;
        int err = dladdr(addr, &info);
        assert(err != 0);

        nohook_string filename = info.dli_fname;

        for (const nohook_string &library_name : _library_names)
            if(filename.find(library_name) != std::string::npos)
                return true;

        return false;
    }

private:

    static int iterate_callback(dl_phdr_info* info, size_t size, void* data)
    {
        library_address_space_resolver* this_ptr = 
            reinterpret_cast<library_address_space_resolver*>(data);

        nohook_string library_name = info->dlpi_name;
        std::cout << "... program uses library: " << library_name << std::endl;

        // Check if name of so file matches any of the libraries we are interested in
        for(const auto& name : this_ptr->_library_names)
        {
            if(library_name.find(name) != std::string::npos)
            {
                std::cout << "   [investigating]" << std::endl;
                // We have one of the libraries we were looking for!
                // Walk all its segments and store the virtual memory ranges
                for(int segment_index = 0; segment_index < info->dlpi_phnum;
                    ++segment_index)
                {
                    char* start_addr = reinterpret_cast<char*>(info->dlpi_addr) 
                                     + info->dlpi_phdr[segment_index].p_vaddr;
                    char* end_addr = start_addr + info->dlpi_phdr[segment_index].p_memsz;

                    this_ptr->_ranges.push_back({reinterpret_cast<void*>(start_addr), 
                                                 reinterpret_cast<void*>(end_addr)});

                    std::cout << " --> mapped to range: "
                            << this_ptr->_ranges.back().begin << " - "
                            << this_ptr->_ranges.back().end << std::endl;
                }
            }
        }

        return 0;
    }

    const nohook_vector<nohook_string> _library_names;
    nohook_vector<address_range> _ranges;
};
